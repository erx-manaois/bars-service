FROM maven:3.9-amazoncorretto-17
COPY . .
RUN mvn clean verify
# ADD target/bars-service-1.0.0.jar app.jar
EXPOSE 8090
ENTRYPOINT [ "java", "-jar", "./target/bars-service-1.0.0.jar" ]
