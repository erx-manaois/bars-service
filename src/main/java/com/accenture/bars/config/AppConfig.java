package com.accenture.bars.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class AppConfig {
    @Value("${bars.app.name}")
    private String appName;

    @Value("${bars.app.version}")
    private String appVersion;

    @Value("${bars.app.desc}")
    private String appDesc;

    @Value("${bars.app.eid}")
    private String eid;

    @Value("${bars.file.directory}")
    private String directory;

    @Override
    public String toString() {
        return "appName='" + appName + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", test directory='" + directory + '\'' +
                ", appDesc='" + appDesc + '\'' +
                ", eid='" + eid + '\'';
    }
}
