package com.accenture.bars.config;

import com.accenture.bars.file.AbstractInputFile;
import com.accenture.bars.file.CSVInputFileImpl;
import com.accenture.bars.file.TextInputFileImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.accenture.bars.file")
public class InputFileConfig {
    @Bean
    @Qualifier("csvInputFileConfig")
    public AbstractInputFile csvInputFileConfig() {
        return new CSVInputFileImpl();
    }

    @Bean
    @Qualifier("txtInputFileConfig")
    public AbstractInputFile txtInputFileConfig() {
        return new TextInputFileImpl();
    }
}
