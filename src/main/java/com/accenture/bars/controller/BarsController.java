package com.accenture.bars.controller;

import com.accenture.bars.config.AppConfig;
import com.accenture.bars.domain.BillingRecord;
import com.accenture.bars.domain.Request;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@RestController
@Slf4j
@NoArgsConstructor
public class BarsController {
    @Autowired
    private FileProcessor fileProcessor;
    @Autowired
    private AppConfig appConfig;

    public BarsController(FileProcessor fileProcessor, AppConfig appConfig) {
        this.fileProcessor = fileProcessor;
        this.appConfig = appConfig;
    }

    @GetMapping(value = "/")
    public String welcome() {
        return appConfig.toString();
    }

    /**
     * Rest Controller for "/bars" endpoint <br/>
     * Method: GET
     *
     * @param filePath filename of the TXT/CSV request file that will be read
     *                 from "bars-test/" that contains the query parameters
     * @return Billing records saved in the database based from the list in the
     *         request file
     * @throws Exception Error during processing of billing request
     */
    @GetMapping("/bars")
    public List<BillingRecord> requestBilling(
            @RequestParam String filePath) throws Exception {
        log.debug("requestBilling()");
        String filename = appConfig.getDirectory() + File.separator + filePath;
        log.debug("filename: " + filename);
        File file = new File(filename);

        List<Request> requests = fileProcessor.execute(file);
        return fileProcessor.retrieveRecordfromDB(requests);
    }

    /**
     * Rest Controller for "/bars" endpoint <br/>
     * Method: POST
     *
     * @param filePath the uploaded TXT/CSV request file included in the
     *                 form-data that contains the query parameters
     * @return Billing records saved in the database based from the list in the
     *         request file
     */
    @PostMapping("/bars")
    public List<BillingRecord> postRequestBilling(
            MultipartFile filePath) throws Exception {
        log.debug("postRequestBilling()");
        List<Request> requests = fileProcessor.execute(filePath);
        return fileProcessor.retrieveRecordfromDB(requests);
    }
}
