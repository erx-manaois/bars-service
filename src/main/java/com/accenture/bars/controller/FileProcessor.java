package com.accenture.bars.controller;

import com.accenture.bars.domain.BillingRecord;
import com.accenture.bars.domain.Request;
import com.accenture.bars.entity.Account;
import com.accenture.bars.entity.Billing;
import com.accenture.bars.entity.Customer;
import com.accenture.bars.exception.BarsException;
import com.accenture.bars.factory.InputFileFactory;
import com.accenture.bars.file.AbstractInputFile;
import com.accenture.bars.repository.BillingRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NoResultException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
@NoArgsConstructor
public class FileProcessor {
    @Autowired
    private BillingRepository billingRepository;

    public FileProcessor(BillingRepository billingRepository) {
        this.billingRepository = billingRepository;
    }

    /**
     * Executes the reading of the TXT/CSV request file located in the local
     * folder of the user
     *
     * @param file The TXT/CSV file that contains the request parameters
     * @return List of request parameters of the file
     * @throws Exception
     */
    public List<Request> execute(File file) throws Exception {
        log.debug("execute()");
        if (!file.exists()) {
            throw new BarsException(BarsException.PATH_DOES_NOT_EXIST);
        }
        AbstractInputFile inputFile = InputFileFactory.getInputFile(file);

        return inputFile.readFile();
    }

    /**
     * Executes the reading of the TXT/CSV request file that is included
     * in the HTTP request message
     *
     * @param file The TXT/CSV file that contains the request parameters
     * @return List of request parameters of the file
     * @throws Exception
     */
    public List<Request> execute(MultipartFile file) throws Exception {
        log.debug("execute()");
        AbstractInputFile inputFile = InputFileFactory.getInputFile(
                getFileFromInputStream(file.getInputStream(), file));

        return inputFile.readFile();
    }

    /**
     * Retrieves the record from the database based on the request parameter
     * file
     *
     * @param requests List of request parameters
     * @return Records retrieved from the database
     * @throws Exception
     */
    public List<BillingRecord> retrieveRecordfromDB(List<Request> requests)
            throws Exception {
        log.debug("retrieveRecordfromDB()");
        List<BillingRecord> records = new ArrayList<>();
        for (Request request : requests) {
            List<Billing> billings = new ArrayList<>();

            try {
                billings = billingRepository
                        .findByBillingCycleAndStartDateAndEndDate(
                                request.billingCycle(),
                                request.startDate(),
                                request.endDate());
            } catch (NoResultException | EmptyResultDataAccessException e) {
                throw new BarsException(BarsException.NO_RECORDS_TO_WRITE, e);
            }
            for (Billing billing : billings) {
                Account account = billing.getAccount();
                Customer customer = account.getCustomer();

                BillingRecord rec = new BillingRecord(
                        billing.getBillingCycle(),
                        billing.getStartDate(),
                        billing.getEndDate(),
                        account.getAccountName(),
                        customer.getFirstName(),
                        customer.getLastName(),
                        billing.getAmount());

                records.add(rec);
            }
        }

        if (records.isEmpty()) {
            throw new BarsException(BarsException.NO_RECORDS_TO_WRITE);
        }

        log.debug("Records: " + records);
        return records;
    }

    private File getFileFromInputStream(InputStream inputStream,
            MultipartFile filePath)
            throws Exception {
        final int DEFAULT_BUFFER_SIZE = 10000;
        File file = new File(filePath.getOriginalFilename());

        try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
            int read;
            byte[] bytes = new byte[DEFAULT_BUFFER_SIZE];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            throw new BarsException(BarsException.PATH_DOES_NOT_EXIST, e);
        }
        return file;
    }
}
