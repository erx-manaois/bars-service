package com.accenture.bars.domain;

import java.time.LocalDate;

public record BillingRecord(
        int billingCycle,
        LocalDate startDate,
        LocalDate endDate,
        String accountName,
        String firstName,
        String lastName,
        double amount) {

}
