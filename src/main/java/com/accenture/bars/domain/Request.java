package com.accenture.bars.domain;

import java.time.LocalDate;

public record Request(
        int billingCycle,
        LocalDate startDate,
        LocalDate endDate) {

}
