package com.accenture.bars.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "account")
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"customer", "billing"})
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private int accountId;

    @Column(name = "account_name")
    private String accountName;

    @Column(name = "date_created")
    private LocalDateTime dateCreated;

    @Column(name = "is_active")
    private String isActive;

    @Column(name = "last_edited")
    private String lastEdited;

    @ManyToOne(cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "customer_id")
    private Customer customer;
        
    @OneToMany(mappedBy = "account", cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    private Set<Billing> billing;

    public Account(String accountName, LocalDateTime dateCreated,
                   String isActive, String lastEdited) {
        this.accountName = accountName;
        this.dateCreated = dateCreated;
        this.isActive = isActive;
        this.lastEdited = lastEdited;
    }
}
