package com.accenture.bars.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "customer")
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = {"account"})
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private int customerId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "address")
    private String address;

    @Column(name = "status")
    private String status;

    @Column(name = "dateCreated")
    private LocalDateTime dateCreated;

    @Column(name = "lastEdited")
    private String lastEdited;

    @OneToMany(mappedBy = "customer", cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    private Set<Account> account;

    public Customer(String firstName, String lastName, String address,
                    String status, LocalDateTime dateCreated,
                    String lastEdited) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.status = status;
        this.dateCreated = dateCreated;
        this.lastEdited = lastEdited;
    }
}
