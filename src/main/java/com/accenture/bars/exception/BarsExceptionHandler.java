package com.accenture.bars.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;

@ControllerAdvice
@Slf4j
public class BarsExceptionHandler {
    private HttpServletRequest request;

    public BarsExceptionHandler(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Handles the exception that will result from the application processing.
     * Error message will be shown in the HTTP response.
     *
     * @param e Exception
     * @return ErrorResponse to the user
     */
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> getResponseEntity(Exception e) {
        log.error("ERROR: " + getStackTrace(e));
        HttpStatus errorStatus = HttpStatus.BAD_REQUEST;

        ErrorResponse errorResponse = new ErrorResponse(
                LocalDateTime.now(),
                errorStatus.value(),
                errorStatus.getReasonPhrase(),
                e.getMessage(),
                request.getServletPath());

        return new ResponseEntity<>(errorResponse, errorStatus);
    }

    private String getStackTrace(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
