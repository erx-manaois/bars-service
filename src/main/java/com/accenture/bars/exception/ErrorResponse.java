package com.accenture.bars.exception;

// import lombok.Getter;
// import lombok.Setter;

import java.time.LocalDateTime;

public record ErrorResponse(
        LocalDateTime timeStamp,
        int status,
        String error,
        String message,
        String path) {

}
