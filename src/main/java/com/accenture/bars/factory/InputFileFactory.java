package com.accenture.bars.factory;

import com.accenture.bars.config.InputFileConfig;
import com.accenture.bars.exception.BarsException;
import com.accenture.bars.file.AbstractInputFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.File;
import java.util.Optional;

@Slf4j
public class InputFileFactory {

    private static InputFileFactory factory = null;

    private InputFileFactory() {
    }

    /**
     * Singleton instance of InputFileFactory
     *
     * @return Instance of InputFileFactory
     */
    public static InputFileFactory getInstance() {
        if (factory == null) {
            factory = new InputFileFactory();
        }
        return factory;
    }

    /**
     * Reads the input file
     *
     * @param file Request parameter file
     * @return Input file
     * @throws Exception Error during processing of billing request
     */
    public static AbstractInputFile getInputFile(File file) throws Exception {
        log.debug("getInputFile()");
        AbstractInputFile inputFile = null;
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(InputFileConfig.class);
        try {
            switch (getFileExtensionName(file.getName())) {
                case "csv":
                    inputFile = applicationContext.getBean("csvInputFileConfig",
                            AbstractInputFile.class);
                    break;
                case "txt":
                    inputFile = applicationContext.getBean("txtInputFileConfig",
                            AbstractInputFile.class);
                    break;
                default:
                    throw new BarsException(BarsException.FILE_NOT_SUPPORTED);
            }
        } finally {
            ((ConfigurableApplicationContext) applicationContext).close();
        }
        inputFile.setFile(file);
        return inputFile;
    }

    public static String getFileExtensionName(String filename) {
        log.debug("getFileExtensionName()");
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.toLowerCase().substring(filename.lastIndexOf(".") + 1))
                .orElse("");
    }
}
