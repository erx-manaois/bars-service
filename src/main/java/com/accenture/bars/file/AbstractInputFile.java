package com.accenture.bars.file;

import com.accenture.bars.domain.Request;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.File;
import java.util.List;


@NoArgsConstructor
public abstract class AbstractInputFile {
    public static final int MIN_BILLING_CYCLE = 1;
    public static final int MAX_BILLING_CYCLE = 12;

    @Getter
    @Setter
    private File file;

    public abstract List<Request> readFile() throws Exception;

}
