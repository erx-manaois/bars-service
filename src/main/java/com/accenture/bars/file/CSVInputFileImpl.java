package com.accenture.bars.file;

import com.accenture.bars.domain.Request;
import com.accenture.bars.exception.BarsException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@NoArgsConstructor
public class CSVInputFileImpl extends AbstractInputFile {
    private static final String COMMA_DELIMITER = ",";

    private static final int BILLING_CYCLE = 0;
    private static final int START_DATE = 1;
    private static final int END_DATE = 2;

    /**
     * Reads the CSV parameter file
     *
     * @return List of all request parameters from the file
     * @throws Exception Error during reading of request parameter file
     */
    @Override
    public List<Request> readFile() throws Exception {
        log.debug("CSVInputFileImpl readFile()");
        List<Request> requests = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(
                new FileReader(this.getFile()))) {
            int row = 0;
            String line;
            while ((line = br.readLine()) != null) {
                row++;
                String[] values = line.split(COMMA_DELIMITER);

                requests.add(DataValidator.validateCsvFile(
                        values[BILLING_CYCLE],
                        values[START_DATE],
                        values[END_DATE], row));
            }
        }

        if (requests.isEmpty()) {
            throw new BarsException(BarsException.NO_REQUESTS_TO_READ);
        }

        log.debug("readCSVFile requests: " + requests);
        return requests;
    }
}
