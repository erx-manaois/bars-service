package com.accenture.bars.file;

import com.accenture.bars.domain.Request;
import com.accenture.bars.exception.BarsException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Helper class to validate the request parameter file format
 */
public class DataValidator {
    private DataValidator() {
    }

    public static Request validateCsvFile(String billCycle, String startDate,
            String endDate, int row)
            throws Exception {
        return validate(billCycle, startDate, endDate, row, "MM/dd/yyyy");
    }

    public static Request validateTxtFile(String billCycle, String startDate,
            String endDate, int row)
            throws Exception {
        return validate(billCycle, startDate, endDate, row, "MMddyyyy");
    }

    private static Request validate(
            String billCycle,
            String startDate,
            String endDate,
            int row,
            String format) throws Exception {

        int billingCycle;
        LocalDate startingDate;
        LocalDate endingDate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        // Validate Billing Cycle
        try {
            billingCycle = Integer.parseInt(billCycle);
            if (billingCycle > AbstractInputFile.MAX_BILLING_CYCLE
                    || billingCycle < AbstractInputFile.MIN_BILLING_CYCLE) {
                throw new BarsException(
                        BarsException.BILLING_CYCLE_NOT_ON_RANGE + row);
            }
        } catch (NumberFormatException e) {
            throw new BarsException(BarsException.INVALID_BILLING_CYCLE + row);
        }

        // Validate Start Date
        try {
            startingDate = LocalDate.parse(startDate, formatter);
        } catch (DateTimeParseException e) {
            throw new BarsException(
                    BarsException.INVALID_START_DATE_FORMAT + row, e);
        }

        // Validate End Date
        try {
            endingDate = LocalDate.parse(endDate, formatter);
        } catch (DateTimeParseException e) {
            throw new BarsException(
                    BarsException.INVALID_END_DATE_FORMAT + row, e);
        }

        return new Request(billingCycle, startingDate, endingDate);
    }
}
