package com.accenture.bars.file;

import com.accenture.bars.domain.Request;
import com.accenture.bars.exception.BarsException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@NoArgsConstructor
public class TextInputFileImpl extends AbstractInputFile {
    private static final int LINE_LENGTH = 18;
    private static final int BILL_CYCLE_START = 0;
    private static final int BILL_CYCLE_END = 2;

    private static final int START_DATE_START = 2;
    private static final int START_DATE_END = 10;
    private static final int END_DATE_START = 10;

    /**
     * Reads the TXT parameter file
     *
     * @return List of all request parameters from the file
     * @throws Exception Error during reading of request parameter file
     */
    @Override
    public List<Request> readFile() throws Exception {
        log.debug("TextInputFileImpl readFile()");
        List<Request> requests = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(
                new FileReader(this.getFile()))) {
            int row = 0;
            String line;
            while ((line = br.readLine()) != null) {
                row++;
                line = line.strip();
                int length = line.length();

                if (line.isBlank() || line.isEmpty()) {
                    continue;
                } else if (length != LINE_LENGTH) {
                    throw new BarsException(BarsException.NO_SUPPORTED_FILE);
                }

                requests.add(DataValidator.validateTxtFile(
                        line.substring(BILL_CYCLE_START, BILL_CYCLE_END),
                        line.substring(START_DATE_START, START_DATE_END),
                        line.substring(END_DATE_START), row));
            }
        }

        if (requests.isEmpty()) {
            throw new BarsException(BarsException.NO_REQUESTS_TO_READ);
        }

        return requests;
    }
}
