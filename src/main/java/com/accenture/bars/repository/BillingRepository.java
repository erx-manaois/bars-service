package com.accenture.bars.repository;

import com.accenture.bars.entity.Billing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.time.LocalDate;
import java.util.List;

@RepositoryRestResource(path = "billings")
public interface BillingRepository extends JpaRepository<Billing, Integer> {

    @Query("SELECT b FROM Billing b " +
            "WHERE b.billingCycle = :billingCycle " +
            "AND b.startDate = :startDate " +
            "AND b.endDate = :endDate")
    public List<Billing> findByBillingCycleAndStartDateAndEndDate(
            @Param("billingCycle") int billingCycle,
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate);
}
