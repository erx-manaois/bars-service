package com.accenture.bars.factory;

import com.accenture.bars.exception.BarsException;
import com.accenture.bars.file.CSVInputFileImpl;
import com.accenture.bars.file.TextInputFileImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static com.accenture.bars.exception.BarsException.FILE_NOT_SUPPORTED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class InputFileFactoryTest {

    /**
     * Instantiate an InputFileFactory testing
     *
     * Test Condition:
     * Call InputFileFactory.getInstance()
     *
     * Expected Result:
     * 1) Method must return an instance of InputFileFactory.
     * 2) No exception thrown.
     */
    @Test(expected = Test.None.class)   //Test if result has no exception
    public void getInstanceTest() {
        //When
        //Instantiate an InputFileFactory
        //Call InputFileFactory.getInstance()
        InputFileFactory inputFileFactory = InputFileFactory.getInstance();

        //Then
        //Test if an InputFileFactory instance is created (not null)
        assertNotNull(inputFileFactory);

        //Try to run again getInstance() if it will return the same instance
        InputFileFactory inputFileFactoryNew = InputFileFactory.getInstance();
        assertEquals(inputFileFactory, inputFileFactoryNew);
    }

    /**
     * Get TextInputFileImpl instance testing
     *
     * Test Condition:
     * InputFile object should be an instance of TextInputFileImpl.
     * Call InputFileFactory.getInputFile (myTextFile).
     *
     * Expected Results:
     * 1) Method must return an instance of TextInputFileImpl.
     * 2) No exception thrown.
     */
    @Test(expected = Test.None.class)   //Test if result has no exception
    public void get_TextInputFileImpl_Test() throws Exception {
        //Given
        final String filename = "myTextFile.txt";
        File file = new File(filename);

        //When
        TextInputFileImpl inputFile =
                (TextInputFileImpl) InputFileFactory.getInputFile(file);

        //Then
        assertNotNull(inputFile);
    }

    /**
     * Get CSVInputFileImpl instance
     *
     * Test Conditions:
     * InputFile object should be an instance of CsvInputFileImpl.
     * Call InputFileFactory.getInputFile (myCsvFile).
     *
     * Expected Results:
     * 1) Method must return an instance of CsvInputFileImpl.
     * 2) No exception thrown.
     */
    @Test(expected = Test.None.class)   //Test if result has no exception
    public void get_CsvInputFileImpl_Test() throws Exception {
        //Given
        final String filename = "myCsvFile.csv";
        File file = new File(filename);

        //When
        CSVInputFileImpl inputFile =
                (CSVInputFileImpl) InputFileFactory.getInputFile(file);

        //Then
        assertNotNull(inputFile);
    }

    /**
     * Prompt "File is not supported for processing"
     *
     * Test Condition:
     * Call InputFileFactory.getInputFile (notSupportedFile)
     *
     * Expected Result:
     * 1) BarsException should be thrown indicating that the file is not
     * supported.
     */
    @Test
    public void get_NotSupportedFileImpl_Test() throws Exception {
        //Given
        final String expectedErrorMsg = FILE_NOT_SUPPORTED;
        final String filename = "notSupportedFile.xml";
        File file = new File(filename);

        //When
        BarsException exception = assertThrows(
                BarsException.class,
                () -> {
                    // Test trigger
                    InputFileFactory.getInputFile(file);
                }
        );

        //Then
        //Test if FILE_NOT_SUPPORTED error message in BarsException
        assertEquals(expectedErrorMsg, exception.getMessage());
    }
}
