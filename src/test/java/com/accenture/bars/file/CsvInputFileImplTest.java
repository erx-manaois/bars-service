package com.accenture.bars.file;

import com.accenture.bars.domain.Request;
import com.accenture.bars.exception.BarsException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.accenture.bars.exception.BarsException.BILLING_CYCLE_NOT_ON_RANGE;
import static com.accenture.bars.exception.BarsException.INVALID_END_DATE_FORMAT;
import static com.accenture.bars.exception.BarsException.INVALID_START_DATE_FORMAT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class CsvInputFileImplTest {
    private final String filePath = "bars-test/";
    private final DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * Read Valid Request Parameter
     *
     * Test Condition:
     * Read valid-csv.csv
     * Call CsvInputFileImpl.readFile()
     *
     * Expected Result:
     * 1) Method must return a List<Request> containing 2 elements
     * 2) No exception thrown.
     */
    @Test(expected = Test.None.class)   //Test if result has no exception
    public void readValidReqParameterTest() throws Exception {
        //Given
        final String filename = "valid-csv.csv";
        File file = new File(filePath + filename);
        CSVInputFileImpl inputFile = new CSVInputFileImpl();
        inputFile.setFile(file);

        List<Request> expectedRequests = new ArrayList<>();
        expectedRequests.add(new Request(1,
                LocalDate.parse("2013-01-15", formatter),
                LocalDate.parse("2013-02-14", formatter)));
        expectedRequests.add(new Request(1,
                LocalDate.parse("2016-01-15", formatter),
                LocalDate.parse("2016-02-14", formatter)));

        //When
        List<Request> requests = inputFile.readFile();

        //Then
        //Test if 2 requests are read from the file
        assertEquals(2, requests.size());

        //Test the content of the file based on the expectedRequests values
        assertEquals(expectedRequests.get(0), requests.get(0));
        assertEquals(expectedRequests.get(1), requests.get(1));
    }

    /**
     * Read Invalid Request With Invalid Billing Cycle
     *
     * Test Condition:
     * Read billing-cycle-not-on-range-csv.csv file
     * Call CsvInputFileImpl.readFile()
     *
     * Expected Result:
     * 1) BarsException should be thrown indicating that the Billing Cycle
     * is not on range.
     */
    @Test
    public void readInvalidReq_invalidBillingCycle_test() throws Exception {
        final String filename = "billing-cycle-not-on-range-csv.csv";
        final String errorRow = "4";
        final String expectedErrorMsg = BILLING_CYCLE_NOT_ON_RANGE + errorRow;

        //Given
        CSVInputFileImpl inputFile = setupFileToTest(filename);

        //When
        BarsException exception = assertThrows(
                BarsException.class,
                () -> {
                    inputFile.readFile();
                }
        );

        //Then
        assertEquals(expectedErrorMsg, exception.getMessage());
    }

    /**
     * Read Invalid Request With Invalid Start Date Format
     *
     * Test Condition:
     * Read invalid-start-date-csv.csv
     * Call CsvInputFileImpl.readFile()
     *
     * Expected Result:
     * 1) BarsException should be thrown indicating that the Start Date
     * format is invalid.
     */
    @Test
    public void readInvalidReq_invalidStartDate_test() throws Exception {
        final String filename = "invalid-start-date-csv.csv";
        final String errorRow = "1";
        final String expectedErrorMsg = INVALID_START_DATE_FORMAT + errorRow;

        //Given
        CSVInputFileImpl inputFile = setupFileToTest(filename);

        //When
        BarsException exception = assertThrows(
                BarsException.class,
                () -> {
                    inputFile.readFile();
                }
        );

        //Then
        assertEquals(expectedErrorMsg, exception.getMessage());
    }

    /**
     * Read Invalid Request With Invalid End Date Format
     *
     * Test Condition:
     * Read invalid-end-date-csv.csv
     * Call CsvInputFileImpl.readFile()
     *
     * Expected Result:
     * 1) BarsException should be thrown indicating that the End Date
     * format is invalid.
     */
    @Test
    public void readInvalidReq_invalidEndDate_test() throws Exception {
        final String filename = "invalid-end-date-csv.csv";
        final String errorRow = "7";
        final String expectedErrorMsg = INVALID_END_DATE_FORMAT + errorRow;

        //Given
        CSVInputFileImpl inputFile = setupFileToTest(filename);

        //When
        BarsException exception = assertThrows(
                BarsException.class,
                () -> {
                    inputFile.readFile();
                }
        );

        //Then
        assertEquals(expectedErrorMsg, exception.getMessage());
    }

    private CSVInputFileImpl setupFileToTest(String filename) {
        File file = new File(filePath + filename);
        CSVInputFileImpl inputFile = new CSVInputFileImpl();
        inputFile.setFile(file);

        return inputFile;
    }
}
